<?php 
		
if (!defined('_PS_VERSION_'))
	exit;				
			
class openrateproductfeed extends Module {

private $html = '';
private $productsPerPage = 200;

function __construct() {

	$this->name = 'openrateproductfeed';
	$this->configName = 'OPF';
	$this->tab = 'advertising_marketing';
	$this->version = '1.4';
	$this->author = 'OSCDeveloper.pl Arkadiusz Krakiewicz';
		
	parent::__construct();
	
	$this->displayName = $this->l('Openrate Product Feed');
	$this->description = $this->l('Openrate Product Feed');

}




public function install() {

	if ( 
		!parent::install() ||
		!Configuration::updateValue( $this->configName . '_TOKEN', md5($this->name)) ||
		!Configuration::updateValue( $this->configName . '_PRODUCTS_PER_PAGE', $this->productsPerPage)
	) {
		return false;
	}

	return true;
	
}




public function uninstall() {

	if (
		!parent::uninstall() ||
		!Configuration::deleteByName( $this->configName . '_TOKEN' ) ||
		!Configuration::deleteByName( $this->configName . '_PRODUCTS_PER_PAGE' )
	) {
		return false;
	}
	
	return true;

}




public function getContent() {

	if ( Tools::isSubmit($this->name . 'submit')) {

		Configuration::updateValue( $this->configName . '_TOKEN', Tools::getValue($this->configName . '_TOKEN') );
		Configuration::updateValue( $this->configName . '_PRODUCTS_PER_PAGE', Tools::getValue($this->configName . '_PRODUCTS_PER_PAGE') );

	}
															
	$this->adminContent();
	
	return $this->html;
		
}



 
private function adminContent() {

	$this->html = '
	<style>
	#' . $this->name . 'main,
	#' . $this->name . 'main > div {
		float: left;
		font-size: 16px;
		clear: both;
		margin-top: 10px;
		width: 100%;
	}
	
	#' . $this->name . 'main .' . $this->name . 'logo {
		text-align: center;
		width: 100%;
	}
	
	#' . $this->name . 'main p.' . $this->name . 'info {
		font-size: 11px;
	}
	
	#' . $this->name . 'main p {
		margin: 5px 0 0 0;
		padding: 0;
	}
	
	#' . $this->name . 'main .' . $this->name . 'version {
		font-size: 12px;
		margin-bottom: 0;
	}
	
	#' . $this->name . 'main .' . $this->name . 'bordertop {
		border-top: 1px solid #ccc;
		padding-top: 10px;		
	}
	
	#' . $this->name . 'main .' . $this->name . 'submit {
		text-align: center;
	}
	
	#' . $this->name . 'main a {
		color: #0473BA;
	}
	
	#' . $this->name . 'main a:hover {
		color: #003366;
	}
	</style>
	
	<form id= "' . $this->name . 'form" action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post"> 
	<div id="' . $this->name . 'main">

		<div>
			<p>' . $this->l('Wybrano sklep') . ' <strong>' . Context::getContext()->shop->name . '</strong></p>
			<p class="' . $this->name . 'info">
				' . $this->l('Sklep, dla którego ustawiane są poniższe parametry.') . '
			</p>
		</div>
	
		<div style="display: none;">
			<p>' . $this->l('Ilość produktów na jednej stronie') . ' <input type="text" name="' . $this->configName . '_PRODUCTS_PER_PAGE" value="' . Configuration::get( $this->configName . '_PRODUCTS_PER_PAGE' ) . '" /></p>
			<p class="' . $this->name . 'info">
				' . $this->l('Ilość produktów widocznych przy pojedynczym wywołaniu skryptu z ofertą.') . '
			</p>
		</div>		
		
		<div>
			<p>' . $this->l('Token') . ' <input type="text" name="' . $this->configName . '_TOKEN" value="' . Configuration::get( $this->configName . '_TOKEN' ) . '" style="width: 300px;" /></p>
			<p class="' . $this->name . 'info">
				' . $this->l('Ma na celu zabezpieczenie przed nieporządanymi wywołaniami skryptu, np. flood attack, celowe obciążenie bazy danych.') . '
				<br>
				' . $this->l('Musi zostać podany w adresie URL przy wywołaniu skryptu z ofertą.') . '
			</p>
		</div>
				
		<div class="' . $this->name . 'submit"> 
			<input type="submit" name="' . $this->name . 'submit" value="' . $this->l('Zapisz') . '" class="button" />
		</div>
		
		<div class="' . $this->name . 'bordertop">
			<a href="' . _PS_BASE_URL_ . __PS_BASE_URI__ . 'modules/' . $this->name . '/product_feed.php?token=' . Configuration::get( $this->configName . '_TOKEN' ) . '" target="_blank">
				' . $this->l('Kliknij tutaj') . '</a>, ' . $this->l('aby zobaczyć ofertę dla sklepu ') . '<strong>' . Context::getContext()->shop->name . '</strong>.
			</a>
		</div> 		
		
		<div class="' . $this->name . 'version ' . $this->name . 'bordertop">
			' . $this->l('Wersja wtyczki: ') . $this->version . '
		</div> 		
		
		<div class="' . $this->name . 'logo">
		
			<img src="' . _PS_BASE_URL_ . _MODULE_DIR_ . $this->name . '/img/logo_openrate.jpg" />
		
		</div>
	
	</div>
	
	</form>
	';
		
}

}