<?php
// Heading
$_['heading_title']    = 'Openrate Product Feed';

// Text 
$_['text_feed']        = 'Product Feeds';
$_['text_success']     = 'Zapisano zmiany w Openrate Product Feed!';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_data_feed']  = 'Url:';
$_['entry_token']  = 'Token:';
$_['entry_version']  = 'Wersja wtyczki:';
$_['entry_offer']  = 'Kliknij tutaj';
$_['entry_offer_2']  = ', aby zobaczyć ofertę.';

// Error
$_['error_permission'] = 'Uwaga: Nie masz uprawnienń do modyfikacji Openrate Product Feed!';
?>