<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/feed.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="openrate_product_feed_status">
                <?php if ($openrate_product_feed_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_token; ?></td>
            <td><input type="text" style="width: 300px;" name="openrate_product_feed_token" value="<?php echo $data_token; ?>"/></td>
          </tr>					
          <tr>
            <td><?php echo $entry_data_feed; ?></td>
            <td><a href="<?php echo $data_feed; ?>" target="_blank"><?php echo $entry_offer; ?></a><?php echo $entry_offer_2; ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo $entry_version; ?> <?php echo $data_version; ?></td>
          </tr>				
        </table>
				<input type="hidden" name="openrate_product_feed_version" value="<?php echo $data_version; ?>"/>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>