<?php
class ControllerFeedOpenrateProductFeed extends Controller {

public function index() {

	$token = $this->config->get('openrate_product_feed_token');

	if ( $token != $this->request->get['token'] ) {

		echo '
		<!DOCTYPE html>
		<html>
		<header>
		<meta charset="utf-8">
		<title>Wtyczka Openrate</title>
		<meta name="Author" content="oscDeveloper.pl Arkadiusz Krakiewicz" />
		</header>
		<body>
		<h1>Wtyczka Openrate</h1>
		<p>
		Hash tokena = ' . md5($token) . '
		</p>
		<p>
		Wersja wtyczki = ' . $this->config->get('openrate_product_feed_version') . '
		</p>
		</body>
		</html>
		';

		die;

	}

	
	
	
$this->load->model('catalog/product');
$this->load->model('catalog/category');




if ( isset($this->request->get['product_id']) && (int)$this->request->get['product_id'] > 0 ) {
// only single product

	$productsPerPage = 1;
	$page = 1;
	$totalPages = 1;
	$productsFrom = 0;
	
	$product = $this->model_catalog_product->getProduct($this->request->get['product_id']);

	if ( $product === false && $product['status'] == 0 ) {

		header("HTTP/1.0 404 Not Found");
		
		die;

	}
	
	$products = array();
	$products[0] = $product;

} else {
// all products

	$page = 1;

	if ( isset($this->request->get['page']) && (int)$this->request->get['page'] > 0 ) {
		
		$page = (int)$this->request->get['page'];
		
	}

	$productsPerPage = 7;

	if ( isset($this->request->get['products']) && (int)$this->request->get['products'] > 0 ) {

		$productsPerPage = (int)$this->request->get['products'];

	}

	$productsFrom = $page * $productsPerPage - $productsPerPage;
	
	$products = $this->model_catalog_product->getProducts();

	$totalProducts = count($products);

	$totalPages = $this->roundUp($totalProducts / $productsPerPage);

	if ( $page > $totalPages ) {

		header("HTTP/1.0 404 Not Found");
		
		die;

	}
	
	$products = $this->model_catalog_product->getProducts(
		array(
			'start' => $productsFrom,
			'limit' => $productsPerPage
		)
	);

}




// json
	$json = array();
	
	$json['shop']['offer-info']['products-per-page'] = $productsPerPage;
	$json['shop']['offer-info']['page'] = $page;
	$json['shop']['offer-info']['total-pages'] = $totalPages;

	$json['shop']['shop-info']['shop-name'] = $this->string2json($this->config->get('config_name'));
	$json['shop']['shop-info']['shop-www'] = $this->string2json(HTTP_SERVER);

	$i=0;	
	
	foreach ($products as $product) {

		$product['features'] = $this->model_catalog_product->getProductAttributes($product['product_id']);
		$product['attributes'] = $this->model_catalog_product->getProductOptions($product['product_id']);
		$product['images'] = $this->model_catalog_product->getProductImages($product['product_id']);
		
		$product['url'] = $this->url->link('product/product', 'product_id=' . $product['product_id']);
		
		if ( $product['special'] != '' ) $product['price'] = $product['special'];
		$product['price'] = $this->priceFormat($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
		
		$json['products'][$i]['id'] = $this->string2json($product['product_id']);
		$json['products'][$i]['name'] = $this->string2json($product['name']);
		$json['products'][$i]['url'] = $this->string2json($product['url']);
		$json['products'][$i]['price'] = $this->string2json($product['price']);
		$json['products'][$i]['currency'] = $this->string2json($this->currency->getCode());		
		if ( $product['model'] != '' ) $json['products'][$i]['code'] = $this->string2json($product['model']);
		if ( $product['description'] != '' ) $json['products'][$i]['description'] = $this->string2json($product['description']);
		if ( $product['manufacturer'] != '' ) $json['products'][$i]['manufacturer'] = $this->string2json($product['manufacturer']);
		if ( $this->config->get('config_stock_checkout') ) $json['products'][$i]['quantity'] = $this->string2json($product['quantity']);
		
		if ( count($product['images']) > 0 ) {
		
			$json['products'][$i]['images']['image-main'] = $this->string2json(HTTP_SERVER . 'image/' . $product['images'][0]['image']);
			
			unset($product['images'][0]);
			
			if ( count($product['images']) > 0 ) {
			
				foreach ( $product['images'] as $image ) {

					$json['products'][$i]['images']['image-additional'][] = $this->string2json(HTTP_SERVER . 'image/' . $image['image']);
				
				}
			
			}
		
		}

		if ( count($product['features']) > 0 ) {
		
			$j=0;
			
			foreach ( $product['features'] as $feature ) {
				
				foreach ( $feature['attribute'] as $attribute ) {
					
					$json['products'][$i]['features'][$j]['feature']['type'] = $this->string2json('single');
					$json['products'][$i]['features'][$j]['feature']['name'] = $feature['name'] . ', ' . $attribute['name'];
					$json['products'][$i]['features'][$j]['feature']['values']['value'] = $attribute['text'];
				
					$j++;
				
				}
						
			}
		
		}
		
		if ( count($product['attributes']) > 0 ) {
			
			$quantity = 0;
			
			$j = 0;
			
			foreach ( $product['attributes'] as $attributes ) {
				
				if ( is_array($attributes['option_value']) && count($attributes['option_value']) > 0 ) {
				
					foreach ( $attributes['option_value'] as $option ) {
						
						$price = $product['price'];
						$option['price'] = $this->tax->calculate($option['price'], $product['tax_class_id'], $this->config->get('config_tax'));

						switch ( $option['price_prefix'] ) {
							
							case '-':
								$price -= $option['price'];
							break;
							
							case '/':
								$price /= $option['price'];
							break;

							case '*':
								$price *= $option['price'];
							break;

							default:
								$price += $option['price'];
							break;
						
						}
					
						$json['products'][$i]['attributes'][$j]['attribute']['price'] = $this->string2json($this->priceFormat($price));
						if ( $this->config->get('config_stock_checkout') ) {
							$json['products'][$i]['attributes'][$j]['attribute']['quantity'] = $this->string2json($option['quantity']);
							$quantity += $option['quantity'];
						}
						$json['products'][$i]['attributes'][$j]['attribute']['attribute-versions']['attribute-version']['name'] = $this->string2json($attributes['name']);						
						$json['products'][$i]['attributes'][$j]['attribute']['attribute-versions']['attribute-version']['value'] = $this->string2json($option['name']);
						
						$j++;
						
					}
				
				}
				
			}	
			
			if ( $this->config->get('config_stock_checkout') ) $json['products'][$i]['quantity'] = $this->string2json($quantity);
		
		}

		$categories = $this->model_catalog_product->getCategories($product['product_id']);

		foreach ( $categories as $category ) {

			$categoryTree = $this->getCategoryTree($category['category_id']);
			
			$json['products'][$i]['category'][] = $categoryTree[0];

		}	
		
		$i++;

	}

if ( isset($this->request->get['file_format']) && $this->request->get['file_format'] == 'json' ) {
// json
	
	echo json_encode($json); 
	
	die;

} else {
// xml

	header('Content-Type: text/xml');

	echo '<?xml version="1.0" encoding="UTF-8"?>
	<shop>
		<offer-info>
			<products-per-page>' . $json['shop']['offer-info']['products-per-page'] . '</products-per-page>
			<page>' . $json['shop']['offer-info']['page'] . '</page>
			<total-pages>' . $json['shop']['offer-info']['total-pages'] . '</total-pages>	
		</offer-info>
		<shop-info>
			<shop-name>' . $this->string2xml($json['shop']['shop-info']['shop-name']) . '</shop-name>
			<shop-www>' . $this->string2xml($json['shop']['shop-info']['shop-www']) . '</shop-www>
		</shop-info>
		<products>
	';	

	foreach ($json['products'] as $product) {
	
		echo '
		<product>
			<id>' . $this->string2xml($product['id']) . '</id>
			<name>' . $this->string2xml($product['name']) . '</name>
			<price>' . $this->string2xml($product['price']) . '</price>
			<currency>' . $this->string2xml($product['currency']) . '</currency>
			<url>' . $this->string2xml($product['url']) . '</url>
		';	
	
		if ( isset($product['code']) ) echo '<code>' . $this->string2xml($product['code']) . '</code>';
		if ( isset($product['description']) ) echo '<description>' . $this->string2xml($product['description']) . '</description>';
		if ( isset($product['manufacturer']) ) echo '<manufacturer>' . $this->string2xml($product['manufacturer']) . '</manufacturer>';
		if ( isset($product['quantity']) ) echo '<quantity>' . $this->string2xml($product['quantity']) . '</quantity>';
		
		if ( isset($product['images']) ) {
			
			echo '
			<images>
				<image-main>' . $this->string2xml($product['images']['image-main']) . '</image-main>
			';
			
			if ( isset($product['images']['image-additional']) ) {
			
				foreach ( $product['images']['image-additional'] as $image ) { 
						
					echo '<image-additional>' . $this->string2xml($image) . '</image-additional>';
				
				}
			
			}
			
			echo '
			</images>
			';
			
		}

		if ( isset($product['features']) ) {
		
			echo '
			<features>	
			';
			
			foreach ( $product['features'] as $feature ) {
				
				echo '
				<feature>
					<name>' . $this->string2xml($feature['feature']['name']) . '</name>
					<type>' . $this->string2xml($feature['feature']['type']) . '</type>
					<values>
						<value>' . $this->string2xml($feature['feature']['values']['value']) . '</value>
					</values>
				</feature>
				'; 
						
			}
			
			echo '
			</features>	
			';			
		
		}
		
		if ( isset($product['attributes']) ) {
		
			echo '
			<attributes>
			';
			
			foreach ( $product['attributes'] as $attribute ) {
			
				echo '
				<attribute>
					<price>' . $this->string2xml($attribute['attribute']['price']) . '</price>
				';
				
				if ( isset($attribute['attribute']['quantity']) ) echo '<quantity>' . $this->string2xml($attribute['attribute']['quantity']) . '</quantity>';
				
				echo '
					<attribute-versions>
						<attribute-version>
							<name>' . $this->string2xml($attribute['attribute']['attribute-versions']['attribute-version']['name']) . '</name>
							<value>' . $this->string2xml($attribute['attribute']['attribute-versions']['attribute-version']['value']) . '</value>
						</attribute-version>
					</attribute-versions>
				</attribute>
				';				
			
			}
			
			echo '
			</attributes>
			';			

		}

		echo '
			<categories>
		';

		foreach ( $product['category'] as $category ) {

			echo '
			<category>
			' . $this->drawCategoryTree2xml($category) . '
			</category>
			';

		}	
		
		echo '
			</categories>
		</product>
		';
		
	}
	
	echo '
		</products>
	</shop>
	';	
	
	die;
	
}
		


}



	
	protected function drawCategoryTree2xml($arr, $depth = 0, $maxDepth = 1000) {
			$retVal = "";
			foreach($arr as $key => $value) {
					$retVal .= '<' . $key . '>';

					$type = gettype($value);
					switch ($type) {
							case "array":
							case "object":
									if ($depth < $maxDepth)
											$retVal .= $this->drawCategoryTree2xml($value, $depth+1, $maxDepth);
									else
											$retVal .= $type;
									break;
							case "boolean":
									$retVal .= $value ? "true" : "false";
									break;
							case "resource":
									$retVal .= "Resource";
							case "NULL":
									$retVal .= "NULL";
							default:
									$retVal .= $this->string2xml($value);
					}
					$retVal .= '</' . $key . '>
					';
			}
			return $retVal;
	}
	
	protected function roundUp($value, $places = 0) {
		$mult = pow(10, abs($places));
		return $places < 0 ?
			ceil($value / $mult) * $mult :
			ceil($value * $mult) / $mult;
	}

	protected function string2json ( $string ) {
		return htmlspecialchars($string, ENT_QUOTES, "UTF-8");
	}

	protected function string2xml($string) {
		return '<![CDATA[' . $string . ']]>';
	}	
	
	protected function linkPreparation( $link ) {
		return parse_url($link, PHP_URL_SCHEME) === null ? 'http://' . $link : $link;
	}	
	
	protected function priceFormat( $price ) {
		return number_format($price, 2, '.', '');
	}	

	protected function getCategoryTree( $id, $category_array = array() ) {
		
		$category = $this->model_catalog_category->getCategory($id);

		if ( $id > 0 ) {
			
			if ( count($category_array) == 0 ) {

				$category_array[0] = array( 'id' => $id, 'name' => $category['name']);				
				
			} else {
				
				$temp = $category_array[count($category_array)-1];
				
				$category_array[count($category_array)-1] = array('id' => $id, 'name' => $category['name']);
				$category_array[count($category_array)-1]['category'] = $temp;
			
			}
			
			$category_array = $this->getCategoryTree($category['parent_id'], $category_array);
			
		}			

		return $category_array;
			
	}

}
?>