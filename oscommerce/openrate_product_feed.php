<?

@set_time_limit(3600);


define('TOKEN','3237fb718cbdbec738cd9f8f1143d77b9fc61928');
define('PLUGIN_VERSION','1.2');


function roundUp($value, $places = 0) {
	$mult = pow(10, abs($places));
	return $places < 0 ?
		ceil($value / $mult) * $mult :
		ceil($value * $mult) / $mult;
}


function priceGross ( $priceNet, $taxValue ) {
	return number_format($priceNet * $taxValue, 2, '.', '');
}

function string2utf ( $string ) {
	if ( strpos(strtolower(CHARSET), 'utf') === false ) {
		$string = iconv(strtoupper(CHARSET), "UTF-8", $string);
	}
	return $string;
}

function string2json ( $string ) {
	return htmlspecialchars(string2utf($string), ENT_QUOTES, "UTF-8");
}

function string2xml ( $string ) {
	return '<![CDATA[' . htmlspecialchars(string2utf($string), ENT_QUOTES, "UTF-8") . ']]>';
}

function getAttribute ( $productsId, $price, $taxClassId ) {

	global $tax, $languages_id;
	
	$i = 0;
	$element = array();
	
	$attributeQuery  = tep_db_query("
	SELECT
		*
	FROM
		" . TABLE_PRODUCTS_ATTRIBUTES . " pa 
		LEFT JOIN " . TABLE_PRODUCTS_OPTIONS . " po ON pa.options_id = po.products_options_id
		LEFT JOIN " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov ON pa.options_values_id = pov.products_options_values_id
	WHERE
		pa.products_id = " . (int)$productsId . "
		AND
		po.language_id = " . (int)$languages_id . "
	");

	while ( $attribute = tep_db_fetch_array($attributeQuery) ) {

		switch ( $attribute['price_prefix'] ) {
			
			case '-':
				$price -= $attribute['options_values_price'];
			break;
			
			case '/':
				$price /= $attribute['options_values_price'];
			break;

			case '*':
				$price *= $attribute['options_values_price'];
			break;

			default:
				$price += $attribute['options_values_price'];
			break;
		
		}
		
		$element[$i]['price'] = priceGross($price, $tax[$taxClassId]);
		$element[$i]['attribute-versions']['attribute-version']['name'] = $attribute['products_options_name'];
		$element[$i]['attribute-versions']['attribute-version']['value'] = $attribute['products_options_values_name'];
		
		$i++;
		
	}

	return $element;

}

function getAdditionalImages ( $productsId ) {
	
	$element = array();
	
	if ( tep_db_num_rows(tep_db_query("SHOW TABLES LIKE 'additional_images'")) == 1 ) {
		
		$imageQuery = tep_db_query("
		SELECT
			popup_images
		FROM
			" . TABLE_ADDITIONAL_IMAGES . "
		WHERE
			products_id = " . (int)$productsId . "
		");
		
		while ( $image = tep_db_fetch_array($imageQuery) ) {
		
			$element[] = HTTP_SERVER . '/' . DIR_WS_IMAGES . $image['popup_images'];
		
		}
	
	}
	
	return $element;

}

function getProductCategory ( $pId ) {
	
	$category = array();
	
	$q = tep_db_query("
	SELECT
		categories_id
	FROM
		" . TABLE_PRODUCTS_TO_CATEGORIES . "
	WHERE
		products_id = " . (int)$pId . "
	");
	
	while ( $f = tep_db_fetch_array($q) ) {
	
		$category[] = $f['categories_id'];
	
	}
	
	return $category;

}


function getCategoryTree( $id, $category_array = array() ) {
	
	global $languages_id;

	$category_query = tep_db_query("
	SELECT 
		cd.categories_name, 
		c.parent_id 
	FROM 
		" . TABLE_CATEGORIES . " c, 
		" . TABLE_CATEGORIES_DESCRIPTION . " cd 
	WHERE 
		c.categories_id = '" . (int)$id . "' 
		AND 
		c.categories_id = cd.categories_id 
		AND 
		cd.language_id = '" . (int)$languages_id . "'
	");
	
	$category = tep_db_fetch_array($category_query);

	if ( $id > 0 ) {
		
		if ( count($category_array) == 0 ) {

			$category_array[0] = array( 'id' => $id, 'name' => $category['categories_name']);				
			
		} else {
			
			$temp = $category_array[count($category_array)-1];
			
			$category_array[count($category_array)-1] = array('id' => $id, 'name' => $category['categories_name']);
			$category_array[count($category_array)-1]['category'] = $temp;
		
		}
		
		$category_array = getCategoryTree($category['parent_id'], $category_array);
		
	}			

	return $category_array;
		
}


function drawCategoryTree2xml($arr, $depth = 0, $maxDepth = 1000) {
    $retVal = "";
    foreach($arr as $key => $value) {
        $retVal .= '<' . $key . '>';

        $type = gettype($value);
        switch ($type) {
            case "array":
            case "object":
                if ($depth < $maxDepth)
                    $retVal .= drawCategoryTree($value, $depth+1, $maxDepth);
                else
                    $retVal .= $type;
                break;
            case "boolean":
                $retVal .= $value ? "true" : "false";
                break;
            case "resource":
                $retVal .= "Resource";
            case "NULL":
                $retVal .= "NULL";
            default:
                $retVal .= string2xml($value);
        }
        $retVal .= '</' . $key . '>
				';
    }
    return $retVal;
}


if ( TOKEN != $_GET['token'] ) {

	echo '
	<!DOCTYPE html>
	<html>
	<header>
	<meta charset="utf-8">
	<title>Wtyczka Openrate</title>
	<meta name="Author" content="oscDeveloper.pl Arkadiusz Krakiewicz" />
	</header>
	<body>
	<h1>Wtyczka Openrate</h1>
	<p>
	Hash tokena = ' . md5(TOKEN) . '
	</p>
	<p>
	Wersja wtyczki = ' . PLUGIN_VERSION . '
	</p>
	</body>
	</html>
	';

	die;

}




require_once('includes/application_top.php');



if ( isset($_GET['product_id']) ) {
// only single product

	$productsPerPage = 1;
	$page = 1;
	$totalPages = 1;
	
	$productQuery = tep_db_query("
	SELECT
		*
	FROM
		" . TABLE_PRODUCTS . " p 
		LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON p.products_id = pd.products_id
		LEFT JOIN " . TABLE_MANUFACTURERS . " m ON p.manufacturers_id = m.manufacturers_id
	WHERE
		p.products_status = 1
		AND
		pd.language_id = '" . (int)$languages_id . "'
		AND 
		p.products_id = " . (int)$_GET['product_id'] . "
	LIMIT 1
	");
	
	if ( tep_db_num_rows($productQuery) == 0 ) {

		header("HTTP/1.0 404 Not Found");
		
		die;

	}

} else {
// all products
	
	$productsIdQuery = '';
	
	$page = 1;

	if ( isset($_GET['page']) ) {
		
		$page = (int)$_GET['page'];
		
	}

	$productsPerPage = 200;

	if ( isset($_GET['products']) && (int)$_GET['products'] > 0 ) {

		$productsPerPage = (int)$_GET['products'];

	}

	$productsFrom = $page * $productsPerPage - $productsPerPage;

	$q = tep_db_query("
	SELECT
		COUNT(*) AS total
	FROM
		" . TABLE_PRODUCTS . "
	WHERE
		products_status = 1
	");

	$f = tep_db_fetch_array($q);

	$totalProducts = $f['total'];

	$totalPages = roundUp($totalProducts / $productsPerPage);


	if ( $page > $totalPages ) {

		header("HTTP/1.0 404 Not Found");
		
		die;

	}

	$productQuery = tep_db_query("
	SELECT
		*
	FROM
		" . TABLE_PRODUCTS . " p 
		LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON p.products_id = pd.products_id
		LEFT JOIN " . TABLE_MANUFACTURERS . " m ON p.manufacturers_id = m.manufacturers_id
	WHERE
		p.products_status = 1
		AND
		pd.language_id = '" . (int)$languages_id . "'
	ORDER BY 
		p.products_id
	LIMIT 
		" . (int)$productsFrom . ", " . (int)$productsPerPage. "
	");
	
}

// start get tax rates
$tax = array();
$tax[0] = 1;

$q = tep_db_query("
SELECT
	*
FROM
	" . TABLE_TAX_RATES . "
");

while ( $f = tep_db_fetch_array($q) ) {

	$tax[ $f['tax_class_id'] ] = 1 + ( $f['tax_rate'] / 100 );

}
// end get tax rates


if ( isset($_GET['file_format']) && $_GET['file_format'] == 'json' ) {
// json
	
	$json = array();
	 
	$json['shop']['offer-info']['products-per-page'] = $productsPerPage;
	$json['shop']['offer-info']['page'] = $page;
	$json['shop']['offer-info']['total-pages'] = $totalPages;
	
	$json['shop']['shop-info']['shop-name'] = string2json(STORE_NAME);
	$json['shop']['shop-info']['shop-www'] = string2json(HTTP_SERVER);
	
	$i=0;
	
	while ( $product = tep_db_fetch_array($productQuery) ) {

		$json['products'][$i]['id'] = string2json($product['products_id']);
		$json['products'][$i]['name'] = string2json($product['products_name']);
		$json['products'][$i]['url'] = string2json(HTTP_SERVER . '/' . ( $product['products_seo_url'] == '' ? $product['products_url'] : $product['products_seo_url'] ));
		$json['products'][$i]['price'] = string2json(priceGross( $product['products_price'], $tax[$product['products_tax_class_id']]));
		$json['products'][$i]['currency'] = string2json(DEFAULT_CURRENCY);		
		if ( $product['products_model'] != '' ) $json['products'][$i]['code'] = string2json($product['products_model']);
		if ( $product['products_description'] != '' ) $json['products'][$i]['description'] = string2json($product['products_description']);
		if ( $product['manufacturers_name'] != '' ) $json['products'][$i]['manufacturer'] = string2json($product['manufacturers_name']);
		if ( $product['products_quantity'] != '' ) $json['products'][$i]['quantity'] = string2json($product['products_quantity']);
		if ( $product['products_image'] != '' ) $json['products'][$i]['images']['image-main'] = string2json(HTTP_SERVER . '/' . DIR_WS_IMAGES . $product['products_image']);				

		$additionalImages = getAdditionalImages($product['products_id']);
		
		if ( count($additionalImages) > 0 ) {
		
			foreach ( $additionalImages as $v ) {
			
				$json['products'][$i]['images']['image-additional'][] = string2json($v);
			
			}
		
		}
		
		$attribute = getAttribute($product['products_id'], $product['products_price'], $product['products_tax_class_id']);
		
		if ( count($attribute) > 0 ) {
		
			$j = 0;
			
			foreach ($attribute as $v) {

				$json['products'][$i]['attribute'][$j]['price'] = string2json($v['price']);
				$json['products'][$i]['attribute'][$j]['attribute-versions']['attribute-version']['name'] = string2json($v['attribute-versions']['attribute-version']['name']);
				$json['products'][$i]['attribute'][$j]['attribute-versions']['attribute-version']['value'] = string2json($v['attribute-versions']['attribute-version']['value']);
				
				$j++;
				
			}
		
		}
		
		$category = getProductCategory($product['products_id']);

		foreach ( $category as $categoryId ) {

			$categoryTree = getCategoryTree($categoryId);
			
			$json['products'][$i]['category'][] = $categoryTree[0];

		}	
		
		$i++;
		
	}
	

	
	echo json_encode($json); 
	
	die;
	
} else {
// xml

	header('Content-Type: text/xml');

	echo '<?xml version="1.0" encoding="UTF-8"?>
	<shop>
		<offer-info>
			<products-per-page>' . $productsPerPage . '</products-per-page>
			<page>' . $page . '</page>
			<total-pages>' . $totalPages . '</total-pages>	
		</offer-info>
		<shop-info>
			<shop-name>' . string2xml(STORE_NAME) . '</shop-name>
			<shop-www>' . string2xml(HTTP_SERVER) . '</shop-www>
		</shop-info>
		<products>
	';


	while ( $product = tep_db_fetch_array($productQuery) ) {

		echo '<product>';
		echo '<id>' . string2xml($product['products_id']) . '</id>';
		echo '<name>' . string2xml($product['products_name']) . '</name>';
		echo '<url>' . string2xml(HTTP_SERVER . '/' . ( $product['products_seo_url'] == '' ? $product['products_url'] : $product['products_seo_url'] )) . '</url>';
		echo '<price>' . string2xml(priceGross( $product['products_price'], $tax[$product['products_tax_class_id']])) . '</price>';
		echo '<currency>' . string2xml(DEFAULT_CURRENCY) . '</currency>';
		echo '<quantity>' . string2xml($product['products_quantity']) . '</quantity>';		
		if ( $product['products_model'] != '' ) echo '<code>' . string2xml($product['products_model']) . '</code>';
		if ( $product['products_description'] != '' ) echo '<description>' . string2xml($product['products_description']) . '</description>';
		if ( $product['manufacturers_name'] != '' ) echo '<manufacturer>' . string2xml($product['manufacturers_name']) . '</manufacturer>';			
			
		if ( $product['products_image'] != '' ) {
			
			echo '
			<images>
				<image-main>' . string2xml(HTTP_SERVER . '/' . DIR_WS_IMAGES . $product['products_image']) . '</image-main>
			';
			
			$additionalImages = getAdditionalImages($product['products_id']);
			
			if ( count($additionalImages) > 0 ) {
			
				foreach ( $additionalImages as $v ) {
				
					echo '
					<image-additional>' . string2xml($v) . '</image-additional>
					';
				
				}
			
			}
			
			echo '
			</images>
			';
			
		}
		
		$attribute = getAttribute($product['products_id'], $product['products_price'], $product['products_tax_class_id']);
		
		if ( count($attribute) > 0 ) {
			
			echo '
			<attributes>
			';
			
			foreach ($attribute as $v) {

				echo '
				<attribute>
					<price>' . string2xml($v['price']) . '</price>
					<attribute-versions>
						<attribute-version>
							<name>' . string2xml($v['attribute-versions']['attribute-version']['name']) . '</name>
							<value>' . string2xml($v['attribute-versions']['attribute-version']['value']) . '</value>
						</attribute-version>
					</attribute-versions>
				</attribute>
				';
				
			}
			
			echo '
			</attributes>
			';
		
		}
		
		echo '
			<categories>
		';
		
		$category = getProductCategory($product['products_id']);

		foreach ( $category as $categoryId ) {

			$categoryTree = getCategoryTree($categoryId);

			echo '
			<category>
			' . drawCategoryTree2xml($categoryTree[0]) . '
			</category>
			';

		}
		
		echo '
			</categories>
		</product>
		';

	}


	echo '	
		</products>
	</shop>
	';
	
	die;
	
}