<?

include(dirname(__FILE__) . '/../../config/config.inc.php');
include(dirname(__FILE__) . '/../../init.php');
include(dirname(__FILE__) . '/openrateproductfeed.php');

$openrateproductfeed = new openrateproductfeed();

$openrateproductfeedToken = Configuration::get( $openrateproductfeed->configName . '_TOKEN' );

if ( $openrateproductfeedToken != Tools::getValue('token') ) {

	echo '
	<!DOCTYPE html>
	<html>
	<header>
	<meta charset="utf-8">
	<title>Wtyczka Openrate</title>
	<meta name="Author" content="oscDeveloper.pl Arkadiusz Krakiewicz" />
	</header>
	<body>
	<h1>Wtyczka Openrate</h1>
	<p>
	Hash tokena = ' . md5($openrateproductfeedToken) . '
	</p>
	<p>
	Wersja wtyczki = ' . $openrateproductfeed->version . '
	</p>
	</body>
	</html>
	';

	die;

}

function linkPreparation( $link ) {
	return parse_url($link, PHP_URL_SCHEME) === null ? 'http://' . $link : $link;
}

function priceFormat( $price ) {
	return number_format($price, 2, '.', '');
}

function roundUp($value, $places = 0) {
	$mult = pow(10, abs($places));
	return $places < 0 ?
		ceil($value / $mult) * $mult :
		ceil($value * $mult) / $mult;
}

function string2json ( $string ) {
	return htmlspecialchars($string, ENT_QUOTES, "UTF-8");
}

function string2xml($string) {
	return '<![CDATA[' . htmlspecialchars($string, ENT_QUOTES, "UTF-8") . ']]>';
}

function getImages() {
	global $product, $link;

	$image = array();
	$images = $product->getImages(LANG);

	foreach ( $images as $v ) {
		
		if ( $v['cover'] == 1 ) {
		
			$image['main'] = linkPreparation($link->getImageLink($product->link_rewrite, $v['id_image'])); 
			
		} else {
		
			$image['additional'][] = linkPreparation($link->getImageLink($product->link_rewrite, $v['id_image'])); 
			
		}

	}
	
	return $image;

}

function getAttributes($product_id, $attributes) {
	
	$attribute = array();
	
	$i = 0;
	foreach ( $attributes as $v ) {
		
		if ( !array_key_exists($v['id_product_attribute'], $attributes) ) {
			$i=0;
		}
		
		$attribute[$v['id_product_attribute']]['id'] = $v['id_product_attribute'];
		$attribute[$v['id_product_attribute']]['price'] = priceFormat(Product::getPriceStatic($product_id, true, $v['id_product_attribute'], 2));
		$attribute[$v['id_product_attribute']]['quantity'] = $v['quantity'];
		$attribute[$v['id_product_attribute']]['version'][$i]['name'] = $v['group_name'];
		$attribute[$v['id_product_attribute']]['version'][$i]['value'] = $v['attribute_name'];
		
		$i++;
		
	}
	
	return $attribute;

}

function getCategoryTree( $id, $category_array = array() ) {
	global $db;
	
	$category_query = "
	SELECT 
		c.id_parent,
		cl.name
	FROM 
		" . _DB_PREFIX_ . "category c
		LEFT JOIN " . _DB_PREFIX_ . "category_lang cl ON (c.id_category = cl.id_category AND cl.id_lang = " . (int)LANG . ")
		" . Shop::addSqlAssociation("category", "c") . "
	WHERE
		c.id_category = " . (int)$id . "
	";
	
	$category = $db->getRow($category_query);

	if ( $id > 0 ) {
		
		if ( count($category_array) == 0 ) {

			$category_array[0] = array( 'id' => $id, 'name' => $category['name']);				
			
		} else {
			
			$temp = $category_array[count($category_array)-1];
			
			$category_array[count($category_array)-1] = array('id' => $id, 'name' => $category['name']);
			$category_array[count($category_array)-1]['category'] = $temp;
		
		}
		
		$category_array = getCategoryTree($category['id_parent'], $category_array);
		
	}			

	return $category_array;
		
}

function drawCategoryTree2xml($arr, $depth = 0, $maxDepth = 1000) {
    $retVal = "";
    foreach($arr as $key => $value) {
        $retVal .= '<' . $key . '>';

        $type = gettype($value);
        switch ($type) {
            case "array":
            case "object":
                if ($depth < $maxDepth)
                    $retVal .= drawCategoryTree2xml($value, $depth+1, $maxDepth);
                else
                    $retVal .= $type;
                break;
            case "boolean":
                $retVal .= $value ? "true" : "false";
                break;
            case "resource":
                $retVal .= "Resource";
            case "NULL":
                $retVal .= "NULL";
            default:
                $retVal .= string2xml($value);
        }
        $retVal .= '</' . $key . '>
				';
    }
    return $retVal;
}




$db = Db::getInstance();

define("LANG", Context::getContext()->language->id);
define("SHOP_ID", Context::getContext()->shop->id);




if ( (int)Tools::getValue('product_id') > 0 ) {
// only single product

	$productsPerPage = 1;
	$page = 1;
	$totalPages = 1;
	$productsFrom = 0;
	
	$q = "
	SELECT 
		COUNT(*)
	FROM 
		" . _DB_PREFIX_ . "product p
		" . Shop::addSqlAssociation("product", "p") . "
	WHERE
		" . ( Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') == 1 ? 'product_shop' : 'p' ) . ".active = 1
		AND
		p.id_product = " . (int)Tools::getValue('product_id') . "
	";

	$totalProducts = $db->getValue($q);
	
	if ( $totalProducts == 0 ) {
		header("HTTP/1.0 404 Not Found");
		die;
	}

} else {
// all products

	$page = 1;
	if ( (int)Tools::getValue('page') > 0 ) {
		$page = (int)Tools::getValue('page');
	}


	$productsPerPage = 200;
	if ( (int)Tools::getValue('products') > 0 ) {
		$productsPerPage = (int)Tools::getValue('products');
	}

	$productsFrom = $page * $productsPerPage - $productsPerPage;

	$q = "
	SELECT 
		COUNT(*)
	FROM
		" . _DB_PREFIX_ . "product p
		" . Shop::addSqlAssociation("product", "p") . "
	WHERE
		" . ( Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') == 1 ? 'product_shop' : 'p' ) . ".active = 1
	";

	$totalProducts = $db->getValue($q);

	$totalPages = roundUp($totalProducts / $productsPerPage);

	if ( $page > $totalPages ) {
		header("HTTP/1.0 404 Not Found");
		die;
	}

}

$products = ProductCore::getProducts(LANG, $productsFrom, $productsPerPage, 'id_product', 'ASC', false, true);


if ( Tools::getValue('file_format') == 'json' ) {
// json

	$json = array();
	
	$json['shop']['offer-info']['products-per-page'] = $productsPerPage;
	$json['shop']['offer-info']['page'] = $page;
	$json['shop']['offer-info']['total-pages'] = $totalPages;
	
	$json['shop']['shop-info']['shop-name'] = string2json(Configuration::get('PS_SHOP_NAME'));
	$json['shop']['shop-info']['shop-www'] = string2json(_PS_BASE_URL_ . __PS_BASE_URI__);

	$i=0;
	
	foreach ($products as $p) {
	
		$product = new Product($p['id_product'], true, LANG, SHOP_ID);
		
		$product->features = $product->getFrontFeatures(LANG, $product->id);
		$product->priceGrossWithDiscount = priceFormat(Product::getPriceStatic($product->id, true, false, 2));
		$product->reference = ($product->reference != '' ? $product->reference : $product->supplier_reference);
		$product->description = ($product->description != '' ? $product->description : $product->description_short);		
		
		$link = new Link();
		
		$product->attributes = getAttributes($product->id, $product->getAttributeCombinations(LANG));	
		$product->images = getImages();

		$json['products'][$i]['id'] = string2json($product->id);
		$json['products'][$i]['name'] = string2json($product->name);
		$json['products'][$i]['url'] = string2json(linkPreparation($link->getProductLink($product)));
		$json['products'][$i]['price'] = string2json($product->priceGrossWithDiscount);
		$json['products'][$i]['currency'] = string2json($currency->iso_code);		
		if ( $product->reference != '' ) $json['products'][$i]['code'] = string2json($product->reference);
		if ( $product->description != ''  ) $json['products'][$i]['description'] = string2json($product->description);
		if ( $product->manufacturer_name != '' ) $json['products'][$i]['manufacturer'] = string2json($product->manufacturer_name);
		if ( Configuration::get('PS_STOCK_MANAGEMENT') ) $json['products'][$i]['quantity'] = string2json($product->quantity);

		if ( count($product->images) > 0 ) {
		
			$json['products'][$i]['images']['image-main'] = string2json($product->images['main']);
			
			if ( isset($product->images['additional']) && count($product->images['additional']) > 0 ) {
			
				foreach ( $product->images['additional'] as $image ) {
				
					$json['products'][$i]['images']['image-additional'][] = string2json($image);
				
				}
			
			}
			
		}

		if ( count($product->features) > 0 ) {
			
			$j = 0;
			
			foreach ( $product->features as $v ) {
				
				$json['products'][$i]['features'][$j]['feature']['name'] = string2json($v['name']);
				$json['products'][$i]['features'][$j]['feature']['type'] = string2json('single');
				$json['products'][$i]['features'][$j]['feature']['values']['value'] = string2json($v['value']);
				
				$j++;
				
			}	
		
		}

		if ( count($product->attributes) > 0 ) {
			
			$j = 0;
			
			foreach ( $product->attributes as $attributes ) {
				
				$json['products'][$i]['attributes'][$j]['attribute']['price'] = string2json($attributes['price']);
				if ( Configuration::get('PS_STOCK_MANAGEMENT') ) $json['products'][$i]['attributes'][$j]['attribute']['quantity'] = string2json($attributes['quantity']);
				
				$z = 0;
				
				foreach ( $attributes['version'] as $version ) {
				
					$json['products'][$i]['attributes'][$j]['attribute']['attribute-versions'][$z]['attribute-version']['name'] = string2json($version['name']);
					$json['products'][$i]['attributes'][$j]['attribute']['attribute-versions'][$z]['attribute-version']['value'] = string2json($version['value']);		
					
					$z++;
					
				}	
				
				$j++;
				
			}	
		
		}

		$category = $product->getCategories();

		foreach ( $category as $categoryId ) {

			$categoryTree = getCategoryTree($categoryId);
			
			$json['products'][$i]['category'][] = $categoryTree[0];

		}

		$i++;
	
	}
	
	echo json_encode($json); 
	
	die;

} else {
// xml

	header('Content-Type: text/xml');

	echo '<?xml version="1.0" encoding="UTF-8"?>
	<shop>
		<offer-info>
			<products-per-page>' . $productsPerPage . '</products-per-page>
			<page>' . $page . '</page>
			<total-pages>' . $totalPages . '</total-pages>	
		</offer-info>
		<shop-info>
			<shop-name>' . string2xml(Configuration::get('PS_SHOP_NAME')) . '</shop-name>
			<shop-www>' . string2xml(_PS_BASE_URL_ . __PS_BASE_URI__) . '</shop-www>
		</shop-info>
		<products>
	';


	foreach ($products as $p) {

		$product = new Product($p['id_product'], true, LANG, SHOP_ID);
		$product->features = $product->getFrontFeatures(LANG, $product->id);
		$product->priceGrossWithDiscount = Product::getPriceStatic($product->id, true, false, 2);
		if ( $product->priceGrossWithDiscount == 0 ) { 
			$product->priceGrossWithDiscount = Product::getPriceStatic($product->id, true, null, 2);
		}
		$product->priceGrossWithDiscount = priceFormat($product->priceGrossWithDiscount);
		$product->reference = ($product->reference != '' ? $product->reference : $product->supplier_reference);
		$product->description = ($product->description != '' ? $product->description : $product->description_short);		
			
		$link = new Link();

		$product->attributes = getAttributes($product->id, $product->getAttributeCombinations(LANG));	
		$product->images = getImages();
		
		echo '
		<product>
			<id>' . string2xml($product->id) . '</id>
			<name>' . string2xml($product->name) . '</name>
			<price>' . string2xml($product->priceGrossWithDiscount) . '</price>
			<currency>' . string2xml($currency->iso_code) . '</currency>
			<url>' . string2xml(linkPreparation($link->getProductLink($product))) . '</url>
		';

		if ( Configuration::get('PS_STOCK_MANAGEMENT') ) echo '<quantity>' . string2xml($product->quantity) . '</quantity>';
		if ( $product->reference != '' ) echo '<code>' . string2xml($product->reference) . '</code>';
		if ( $product->description != '' ) echo '<description>' .  string2xml($product->description) . '</description>';
		if ( $product->manufacturer_name != '' ) echo '<manufacturer>' . string2xml($product->manufacturer_name) . '</manufacturer>';	
		
		if ( count($product->images) > 0 ) {
		
			echo '
			<images>
			<image-main>' . string2xml($product->images['main']) . '</image-main>
			';
			
			if ( isset($product->images['additional']) && count($product->images['additional']) > 0 ) {
			
				foreach ( $product->images['additional'] as $image ) {
				
					echo '<image-additional>' . string2xml($image) . '</image-additional>';
				
				}
			
			}

			echo '
			</images>
			';
			
		}	

		if ( count($product->features) > 0 ) {
			
			echo '
			<features>	
			';
			
			foreach ( $product->features as $v ) {
			
				echo '
				<feature>
					<name>' . string2xml($v['name']) . '</name>
					<type>' . string2xml('single') . '</type>
					<values>
						<value>' . string2xml($v['value']) . '</value>
					</values>
				</feature>
				';
			
			}
			
			echo '
			</features>	
			';		
		
		}

		if ( count($product->attributes) > 0 ) {

			echo '
			<attributes>
			';
			
			foreach ( $product->attributes as $attributes ) {
			
				echo '
				<attribute>
					<price>' . string2xml($attributes['price']) . '</price>
				';
				
				if ( Configuration::get('PS_STOCK_MANAGEMENT') ) echo '<quantity>' . string2xml($attributes['quantity']) . '</quantity>';
				
				echo '
					<attribute-versions>
				';
			
				foreach ( $attributes['version'] as $version ) {
				
					echo '
						<attribute-version>
							<name>' . string2xml($version['name']) . '</name>
							<value>' . string2xml($version['value']) . '</value>
						</attribute-version>
					';					
				
				}
			
				echo '
					</attribute-versions>
				</attribute>
				';		
			
			}
			
			echo '
			</attributes>
			';		
		
		}
		
		echo '
			<categories>
		';
		
		$category = $product->getCategories();

		foreach ( $category as $categoryId ) {

			$categoryTree = getCategoryTree($categoryId);

			echo '
			<category>
			' . drawCategoryTree2xml($categoryTree[0]) . '
			</category>
			';

		}	
		
		echo '
			</categories>
		</product>
		';

		
		
	}

	echo '
		</products>
	</shop>
	';

	die;

}