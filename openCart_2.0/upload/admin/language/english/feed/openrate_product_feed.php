<?php
// Heading
$_['heading_title']    = 'Openrate Product Feed';

// Text
$_['text_feed']        = 'Product Feeds';
$_['text_success']     = 'Success: You have modified Openrate Product Feed!';
$_['text_list']        = 'Layout List';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_data_feed']  = 'Data Feed Url:';
$_['entry_token']  = 'Token:';
$_['entry_version']  = 'Plugin version:';
$_['entry_offer']  = 'Click here';
$_['entry_offer_2']  = ', to view offer.';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Openrate Product Feed!';