<?php
class ControllerFeedOpenrateProductFeed extends Controller {

	private $error = array();

	public function index() {
	
		$this->load->language('feed/openrate_product_feed');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		
			$this->model_setting_setting->editSetting('openrate_product_feed', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL'));
			
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_data_feed'] = $this->language->get('entry_data_feed');
		$data['entry_token'] = $this->language->get('entry_token');
		$data['entry_version'] = $this->language->get('entry_version');
		$data['entry_offer'] = $this->language->get('entry_offer');
		$data['entry_offer_2'] = $this->language->get('entry_offer_2');		

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('feed/openrate_product_feed', 'token=' . $this->session->data['token'], 'SSL')
		);
		
		$data['action'] = $this->url->link('feed/openrate_product_feed', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/feed', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['openrate_product_feed_status'])) {
			$data['openrate_product_feed_status'] = $this->request->post['openrate_product_feed_status'];
		} else {
			$data['openrate_product_feed_status'] = $this->config->get('openrate_product_feed_status');
		}

		if (isset($this->request->post['openrate_product_feed_token'])) {
			$data['data_token'] = $this->request->post['openrate_product_feed_token'];
		} else {
			$data['data_token'] = $this->config->get('openrate_product_feed_token');
		}		
		
		if ( $data['data_token'] == '' ) $data['data_token'] = md5($this->language->get('heading_title'));
		$data['data_feed'] = HTTP_CATALOG . 'index.php?route=feed/openrate_product_feed&token=' . $data['data_token'];
		$data['data_version'] = '1.0';

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('feed/openrate_product_feed.tpl', $data));

	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'feed/openrate_product_feed')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}